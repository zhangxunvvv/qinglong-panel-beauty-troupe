/**
 * @author 作者
 * @create_at 2022-09-07 13:32:56
 * @description 脚本描述
 * @version v1.0.0
 * @rule 美团?
 * @rule 美团
 * @title 美团
 * @public false
 */
 const s = sender
 // console.log('消息', s.param(1))
 let URL = ''
 let ID = ''
 let SECRET = ''
 // const {data:{data:{token}}}=await axios.get(URL+'/open/auth/token',{params:{client_id:ID,client_secret:SECRET}})
 if (!s.param(1)) {
     s.reply(`请输入：
     美团记录ck
     美团查询优惠券
     提取ck软件（美团，京东，饿了么）
     下载链接:https://wwp.lanzoup.com/iBF8M0g0oxcb  提取码 : 4g6r
     `)
 }
 /*hidden*/
 const { body: { data: { token } } } = request({
     url: `${URL}/open/auth/token?client_id=${ID}&client_secret=${SECRET}`,
     method: "get",
     json: true,
     // params: {
     //     client_id: ID,
     //     client_secret: SECRET
     // },
     // allowredirects: false, //禁止重定向
 })
 // console.log(1, token)
 
 // console.log(111,data)
 //
 // const res = request({
 //     url: `${URL}/open/envs`,
 //     method: "POST",
 //     headers: {
 //         Authorization: "Bearer " + token,
 //         "Content-Type": "application/json",
 //         'cache-control': 'no-cache'
 //     },
 //     json: true,
 //     body: [{ name: 'meituan', value: '9655', remarks: '544' }]
 // })
 // console.log(res)
 
 
 
 if (s.param(1) == '记录ck') {
     // sender listen 监听用户回复 可以接超时、group和private
     s.reply("请在20秒内发出ck回复：")
 
     var newS = s.listen(20000)//返回一个sender对象，超时后返回null
 
     let mtCk = getMtToken()
     // console.log(mtCk)
     // console.log(`用户ID：${s.getUserId()}`)
     if (newS == null) {
         s.reply("超时，20秒内未回复。")
     } else {
         if (mtCk.indexOf('token=') > -1) {
             const envs = request({
                 url: `${URL}/open/envs`,
                 method: "POST",
                 headers: {
                     Authorization: "Bearer " + token,
                     "Content-Type": "application/json",
                     'cache-control': 'no-cache'
                 },
                 json: true,
                 body: [{ name: 'meituanCookie', value: mtCk, remarks: s.getUserId() }]
             })
            //  console.log(333, envs)
             if (envs.body.data[0].status == 0) {
                 s.reply(`添加ck成功，正在服务器运行，请一天后查看账号信息有无增加`)
             } else {
                 s.reply(`添加ck失败，请检查您的ck格式，重新发送：美团记录ck`)
             }
         } else {
              s.reply(`添加ck失败，请检查您的ck格式，重新发送：美团记录ck`)
         }
 
     }
 }
 
 
 
 
 if (s.param(1) == '查询优惠券') {
     // console.log(getCk())
     if (getCk() === ';token=') s.reply(`查询ck失败，请检查您的ck格式，发送：美团记录ck`)
     function getCoupon() {
         const res = request({
             url: `https://i.waimai.meituan.com/openh5/coupon/list`,
             method: "POST",
             headers: {
                 'Cookie': getCk(),
                 "Content-Type": "application/x-www-form-urlencoded",
                 'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
             },
             "json": true,
             body: { page_size: 30, page_index: 1 }
         })
 
         // console.log(222, res.body.data.coupon_list)
         // console.log(222, res)
 
         let coupon_list = res.body.data.coupon_list.map(el => {
             return {
                 title: el.title,  // 
                 amount: el.amount,  //面额 
                 price_limit: el.price_limit,  //门槛
                 use_limits: el.use_limits,   //条件
                 valid_time_desc: el.valid_time_desc, //有效期   
                 coupon_logo_text: el.coupon_logo_text,   //提示
             }
         })
         // // 条件：${el.use_limits}
         // 提示：${el.coupon_logo_text}
         console.log(coupon_list.length)
         let msg = `共${coupon_list.length}个
 `
         coupon_list.forEach(el => {
             msg = msg + `${el.title}
 面额：${el.amount}
 门槛：${el.price_limit}
 有效：${el.valid_time_desc}
 
 `
         })
         s.reply(msg)
         // console.log(333, res.body.data[0].status)
         // if (res.body.data[0].status == 0) {
         //     s.reply(`添加ck成功，正在服务器运行，请一天后查看账号信息有无增加`)
         // } else {
         //     s.reply(`添加ck失败，请检查您的ck格式，重新发送：美团记录ck`)
         // }
     }
     getCoupon()
 }
 
 if (s.param(1) == '测试') {
     getMtToken()
 
 }
 function getMtToken() {
     let ck = newS.getContent()
     // let mtCk = ck.match(/token=(\S*)/)[1]
 
     // if (mtCk.indexOf('&') !== -1) {
     //     mtCk = mtCk.slice(0, mtCk.indexOf('&'))
     //     console.log(69, mtCk)
     // }
     return ck + '@'
 }
 
 // 
 function getCk() {
     // console.log(s.getUserId())
     const res = request({
         url: `${URL}/open/envs`,
         method: "GET",
         headers: {
             Authorization: "Bearer " + token,
             "Content-Type": "application/json",
         },
         json: true,
     })
     let myToken = ''
     res.body.data.forEach(el => {
         // console.log(654,el.remarks == s.getUserId(),s.getUserId(),1,el.remarks )
         if (el.remarks == s.getUserId()) {
             myToken = el.value
         } else {
             return false
         }
     })
     myToken = myToken.match(/token=(\S*)@/)[1]
     // console.log(333666,myToken)
     return ';token=' + myToken
 }
 /*hidden*/
 